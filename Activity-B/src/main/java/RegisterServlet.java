import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/jobfinder")
public class RegisterServlet extends HttpServlet {

	public void init() throws ServletException {
		System.out.println("RegisterServlet has been initialized");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String appDiscovery = req.getParameter("app_discovery");
		String dateOfBirth = req.getParameter("date_of_birth");
		String workerType = req.getParameter("worker_type");
		String description = req.getParameter("description");
		
		HttpSession session = req.getSession();
		
		session.setAttribute("firstName", firstName);
		session.setAttribute("lastName", lastName);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("appDiscovery", appDiscovery);
		session.setAttribute("dateOfBirth", dateOfBirth);
		session.setAttribute("workerType", workerType);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
	}
	
	public void destroy(){
		System.out.println("RegisterServlet has been destroyed");
		
	}
}
