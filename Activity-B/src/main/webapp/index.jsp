<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Job Finder</title>
</head>
<body>
	
	<div id= "container">
		<h1> Welcome to Servlet Job Finder!</h1>
		<form action= "jobfinder" method= "post">
			<div>
				<label for= "firstName"> First Name</label>
				<input type= "text" name="firstName" required>
			</div>
			
			<div>
				<label for= "lastName"> Last Name</label>
				<input type= "text" name="lastName" required>
			</div>
			
			<div>
				<label for= "phone"> Phone</label>
				<input type= "tel" name="phone" required>
			</div>
			
			<div>
				<label for= "email"> Email</label>
				<input type= "email" name="email" required>
			</div>
			
			<fieldset>
				<legend>How did you discover the app?</legend>
				
				<input type= "radio" id="friends" name= "app_discovery" value="friends" required>
				<label for="friends">Friends</label>
				<br>

				<input type= "radio" id="social_media" name= "app_discovery" value="socialmedia" required>
				<label for="social_media">Social Media</label>
				<br>

				<input type= "radio" id="others" name= "app_discovery" value="others" required>
				<label for="others">Others</label>
			</fieldset>
			
			<div>
				<label for= "date_of_birth">Date of Birth</label>
				<input type="date" name="date_of_birth" required>
			</div>
			
			<div>
				<label for="worker_type"> Are you an employer or applicant?</label>
				<select id="worker" name= "worker_type">
					<option value="" selected="selected">Select One</option>
					<option value="applicant">Applicant</option>
					<option value="employer">Employer</option>
				</select>
			</div>
			
			<div>
				<label for= "description">Profile Description</label>
				<textarea name= "description" maxlength= "500"></textarea>
			</div>
			<button>Register</button>
		</form>
	
	</div>

</body>

</html>