<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home Page</title>
</head>
<body>
		<h1>Welcome <%=session.getAttribute("fullName") %>! </h1>
		
		<%
			String workerType = session.getAttribute("workerType").toString();
			
			if(workerType.equals("applicant")){
				out.println("Welcome applicant. You may now start looking for your career opportunity.");
			}else{
				out.println("Welcome employer. You may start browsing applicant profiles.");
			}
		%>

</body>

</html>